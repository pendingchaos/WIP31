CFLAGS = -pedantic -Wall -std=c11 -g -Og -Isrc
LDFLAGS =
OUTPUT = wip31

TARGET = $@
PREREQ1 = $<

src = $(wildcard src/*.c) $(wildcard src/opts/*.c)
obj = $(join $(dir $(src)), $(addprefix ., $(notdir $(src:.c=.o))))
dep = $(obj:.o=.d)

.PHONY: all
all: $(OUTPUT)

$(OUTPUT): $(obj)
	@$(CC) $(obj) $(LDFLAGS) -o $(TARGET)

-include $(dep)

.%.d: %.c
	@# -M:  generate Makefile rule
	@# -MT: specify the rule's target
	@$(CPP) $(CFLAGS) $(PREREQ1) -M -MT $(@:.d=.o) >$(TARGET)

.%.o: %.c Makefile
	@$(CC) -c $(PREREQ1) $(CFLAGS) -o $(TARGET)

.PHONY: clean
clean:
	@rm -f $(dep) $(obj) $(OUTPUT)

.PHONY: tests
tests: $(OUTPUT)
	cd tests; ./run_tests.sh

.PHONY: tests-valgrind
tests-valgrind: $(OUTPUT)
	cd tests; ./run_tests_valgrind.sh
