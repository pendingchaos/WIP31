#ifndef CONST_OPS_H
#define CONST_OPS_H

#include "module.h"

//These assume the data types of a and b are the same
const_t const_add_wrap(const_t a, const_t b);
const_t const_add_saturate(const_t a, const_t b);
const_t const_subtract_wrap(const_t a, const_t b);
const_t const_subtract_saturate(const_t a, const_t b);
const_t const_multiply_wrap(const_t a, const_t b);
const_t const_multiply_saturate(const_t a, const_t b);
const_t const_divide_wrap(const_t a, const_t b);
const_t const_divide_saturate(const_t a, const_t b);
const_t const_modulo_wrap(const_t a, const_t b);
const_t const_modulo_saturate(const_t a, const_t b);
const_t const_or(const_t a, const_t b);
const_t const_xor(const_t a, const_t b);
const_t const_and(const_t a, const_t b);
const_t const_not(const_t a);
bool const_eq(const_t a, const_t b);
#endif
