#include "module.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

void id_del(id_t id) {
    free(id.name);
}

static void operand_del(operand_t op) {
    switch (op.type) {
    case OPERAND_VALUE:
    case OPERAND_CONSTANT:
        break;
    case OPERAND_BBLOCK:
        id_del(op.bblock);
        break;
    case OPERAND_FUNC:
        id_del(op.func);
        break;
    }
}

void val_del(val_t* val) {
    uint32_t val_count = val->bblock->val_count;
    val_t** vals = val->bblock->vals;
    
    int64_t index = -1;
    for (uint32_t i = 0; i < val_count; i++) {
        if (vals[i] == val) {
            index = i;
            break;
        }
    }
    assert(index >= 0);
    
    memmove(vals+index, vals+index+1, (val_count-index-1)*sizeof(val_t*));
    val->bblock->vals = realloc(vals, --val->bblock->val_count*sizeof(val_t*));
    
    for (uint16_t i = 0; i < val->operand_count; i++)
        operand_del(val->operands[i]);
    free(val->operands);
    free(val);
}

void bblock_del(bblock_t* bblock) {
    uint32_t bblock_count = bblock->func->bblock_count;
    bblock_t** bblocks = bblock->func->bblocks;
    
    int64_t index = -1;
    for (uint32_t i = 0; i < bblock_count; i++) {
        if (bblocks[i] == bblock) {
            index = i;
            break;
        }
    }
    assert(index >= 0);
    
    memmove(bblocks+index, bblocks+index+1, (bblock_count-index-1)*sizeof(bblock_t*));
    bblock->func->bblocks = realloc(bblocks, --bblock->func->bblock_count*sizeof(bblock_t*));
    
    id_del(bblock->id);
    for (uint32_t i = 0; i < bblock->val_count;)
        val_del(bblock->vals[i]);
    free(bblock->vals);
    free(bblock->callers);
    free(bblock->callees);
    free(bblock);
}

void func_del(func_t* func) {
    uint32_t func_count = func->module->func_count;
    func_t** funcs = func->module->funcs;
    
    int64_t index = -1;
    for (uint32_t i = 0; i < func_count; i++) {
        if (funcs[i] == func) {
            index = i;
            break;
        }
    }
    assert(index >= 0);
    
    memmove(funcs+index, funcs+index+1, (func_count-index-1)*sizeof(func_t*));
    func->module->funcs = realloc(funcs, --func->module->func_count*sizeof(func_t*));
    
    id_del(func->id);
    free(func->arg_dtypes);
    for (uint32_t i = 0; i < func->bblock_count;)
        bblock_del(func->bblocks[i]);
    free(func->bblocks);
    free(func);
}

void module_del(module_t* module) {
    for (uint32_t i = 0; i < module->func_count;)
        func_del(module->funcs[i]);
    free(module->funcs);
    free(module);
}

id_t id_new_nt(const char* str) {
    id_t id;
    
    uint32_t name_len = 0;
    for (const char* c = str; *c && name_len!=65535; c++)
        name_len++;
    
    id.name = malloc(name_len+1);
    memcpy(id.name, str, name_len);
    id.name[name_len] = 0;
    
    return id;
}

dtype_t dtype_new_int(uint16_t bits) {
    return (dtype_t){.base=DTYPE_INT, .bits=bits};
}

dtype_t dtype_new_uint(uint16_t bits) {
    return (dtype_t){.base=DTYPE_UINT, .bits=bits};
}

dtype_t dtype_new_ptr() {
    return (dtype_t){.base=DTYPE_PTR};
}

dtype_t dtype_new_void() {
    return (dtype_t){.base=DTYPE_VOID};
}

dtype_t dtype_new_opaque() {
    return (dtype_t){.base=DTYPE_OPAQUE};
}

bool dtype_eq(dtype_t a, dtype_t b) {
    if (a.base != b.base) return false;
    switch (a.base) {
    case DTYPE_INT:
    case DTYPE_UINT:
        return a.bits == b.bits;
    default:
        return true;
    }
}

const_t const_new_u32(uint32_t val) {
    const_t constant;
    constant.dtype = dtype_new_uint(32);
    constant.data[0] = 0;
    constant.data[1] = val;
    return constant;
}

val_t* val_new(bblock_t* bblock, uint32_t id, dtype_t dtype, opcode_t opcode, uint32_t flags) {
    return val_new_at(bblock, bblock->val_count, id, dtype, opcode, flags);
}

val_t* val_new_at(bblock_t* bblock, uint32_t index, uint32_t id, dtype_t dtype, opcode_t opcode, uint32_t flags) {
    if (index > bblock->val_count) return NULL;
    
    val_t* val = malloc(sizeof(val_t));
    val->bblock = bblock;
    val->id = id;
    val->flags = flags;
    val->version = 0;
    val->opcode = opcode;
    val->dtype = dtype;
    val->operand_count = 0;
    val->operands = NULL;
    
    bblock->vals = realloc(bblock->vals, (bblock->val_count+1)*sizeof(val_t*));
    memmove(bblock->vals+index+1, bblock->vals+index, (bblock->val_count-index)*sizeof(val_t*));
    bblock->vals[index] = val;
    bblock->val_count++;
    
    return val;
}

bblock_t* bblock_new(func_t* func, id_t id, uint32_t flags) {
    bblock_t* block = malloc(sizeof(bblock_t));
    block->func = func;
    block->id = id;
    block->flags = flags;
    block->val_count = 0;
    block->vals = NULL;
    block->caller_count = 0;
    block->callers = NULL;
    block->callee_count = 0;
    block->callees = NULL;
    
    func->bblocks = realloc(func->bblocks, (func->bblock_count+1)*sizeof(bblock_t*));
    func->bblocks[func->bblock_count++] = block;
    
    return block;
}

func_t* func_new(module_t* module, id_t id, dtype_t ret_dtype, uint32_t flags) {
    func_t* func = malloc(sizeof(func_t));
    func->module = module;
    func->id = id;
    func->flags = flags;
    func->arg_count = 0;
    func->arg_dtypes = NULL;
    func->ret_dtype = ret_dtype;
    func->bblock_count = 0;
    func->bblocks = NULL;
    
    module->funcs = realloc(module->funcs, (module->func_count+1)*sizeof(func_t*));
    module->funcs[module->func_count++] = func;
    
    return func;
}

void func_add_arg(func_t* func, dtype_t dtype) {
    func->arg_dtypes = realloc(func->arg_dtypes, (func->arg_count+1)*sizeof(dtype_t));
    func->arg_dtypes[func->arg_count++] = dtype;
}

module_t* module_new(uint32_t flags) {
    module_t* mod = malloc(sizeof(module_t));
    mod->flags = flags;
    mod->func_count = 0;
    mod->funcs = NULL;
    return mod;
}

void val_add_operand(val_t* val, operand_t operand) {
    val->operands = realloc(val->operands, (val->operand_count+1)*sizeof(operand_t));
    val->operands[val->operand_count++] = operand;
}

void val_add_operand_val(val_t* val, uint32_t id, uint32_t version) {
    operand_t op;
    op.type = OPERAND_VALUE;
    op.val = id;
    op.version = version;
    val_add_operand(val, op);
}

void val_add_operand_bblock(val_t* val, const id_t operand) {
    operand_t op;
    op.type = OPERAND_BBLOCK;
    op.bblock = id_copy(operand);
    val_add_operand(val, op);
}

void val_add_operand_func(val_t* val, const id_t operand) {
    operand_t op;
    op.type = OPERAND_FUNC;
    op.func = id_copy(operand);
    val_add_operand(val, op);
}

void val_add_operand_const(val_t* val, const const_t operand) {
    operand_t op;
    op.type = OPERAND_CONSTANT;
    op.constant = operand;
    val_add_operand(val, op);
}

void val_set_operand(val_t* val, uint16_t index, const operand_t operand) {
    operand_del(val->operands[index]);
    val->operands[index] = operand;
}

uint32_t val_get_side_effect_count(const val_t* val) {
    switch (val->opcode) {
    case OPCODE_ALLOCA:
    case OPCODE_COPY:
    case OPCODE_ADD:
    case OPCODE_SUBTRACT:
    case OPCODE_MULTIPLY:
    case OPCODE_DIVIDE:
    case OPCODE_MODULO:
    case OPCODE_ARITH_LSHIFT:
    case OPCODE_ARITH_RSHIFT:
    case OPCODE_LOGICAL_LSHIFT:
    case OPCODE_LOGICAL_RSHIFT:
    case OPCODE_CIRCULAR_LSHIFT:
    case OPCODE_CIRCULAR_RSHIFT:
    case OPCODE_BITWISE_OR:
    case OPCODE_BITWISE_XOR:
    case OPCODE_BITWISE_AND:
    case OPCODE_BITWISE_NOT:
    case OPCODE_LOAD:
    case OPCODE_PHI:
    case OPCODE_ARG:
    case OPCODE_CAST:
    case OPCODE_REINTERPRET:
    case OPCODE_LESS:
    case OPCODE_GREATER:
    case OPCODE_EQUAL: return 0;
    case OPCODE_CALL: return 1; //TODO
    case OPCODE_STORE: 
    case OPCODE_JUMP:
    case OPCODE_JUMP_COND:
    case OPCODE_RETURN: return 1;
    }
    assert(false);
}

val_side_effect_t val_get_side_effect(const val_t* val, uint32_t index) {
    switch (val->opcode) {
    case OPCODE_ALLOCA:
    case OPCODE_COPY:
    case OPCODE_ADD:
    case OPCODE_SUBTRACT:
    case OPCODE_MULTIPLY:
    case OPCODE_DIVIDE:
    case OPCODE_MODULO:
    case OPCODE_ARITH_LSHIFT:
    case OPCODE_ARITH_RSHIFT:
    case OPCODE_LOGICAL_LSHIFT:
    case OPCODE_LOGICAL_RSHIFT:
    case OPCODE_CIRCULAR_LSHIFT:
    case OPCODE_CIRCULAR_RSHIFT:
    case OPCODE_BITWISE_OR:
    case OPCODE_BITWISE_XOR:
    case OPCODE_BITWISE_AND:
    case OPCODE_BITWISE_NOT:
    case OPCODE_LOAD:
    case OPCODE_PHI:
    case OPCODE_ARG:
    case OPCODE_CAST:
    case OPCODE_REINTERPRET:
    case OPCODE_LESS:
    case OPCODE_GREATER:
    case OPCODE_EQUAL: return (val_side_effect_t){SIDE_EFFECT_UNSPECIFIED};
    case OPCODE_CALL: return (val_side_effect_t){SIDE_EFFECT_UNSPECIFIED}; //TODO
    case OPCODE_STORE: 
    case OPCODE_JUMP:
    case OPCODE_JUMP_COND:
    case OPCODE_RETURN: return (val_side_effect_t){SIDE_EFFECT_UNSPECIFIED};
    }
    assert(false);
}

val_t* func_find_value(const func_t* func, const uint32_t id, int64_t version) {
    for (uint32_t i = 0; i < func->bblock_count; i++) {
        bblock_t* block = func->bblocks[i];
        for (uint32_t j = 0; j < block->val_count; j++) {
            val_t* val = block->vals[j];
            if (val->id==id &&
                (version>=0?(val->version==version):true)) {
                return val;
            }
        }
    }
    
    return NULL;
}

bblock_t* func_find_basic_block(const func_t* func, const id_t id) {
    for (uint32_t i = 0; i < func->bblock_count; i++)
        if (id_eq(func->bblocks[i]->id, id))
            return func->bblocks[i];
    return NULL;
}

func_t* module_find_func(const module_t* module, const id_t id) {
    for (uint32_t i = 0; i < module->func_count; i++)
        if (id_eq(module->funcs[i]->id, id))
            return module->funcs[i];
    return NULL;
}

id_t id_copy(const id_t id) {
    uint32_t name_len = 0;
    for (const unsigned char* c = id.name; *c; c++) name_len++;
    
    id_t res;
    res.name = malloc(name_len+1);
    memcpy(res.name, id.name, name_len+1);
    return res;
}

bool id_eq(const id_t a, const id_t b) {
    return strcmp((char*)a.name, (char*)b.name) == 0;
}

static operand_t operand_copy(const operand_t* operand) {
    operand_t res;
    res.type = operand->type;
    
    switch (operand->type) {
    case OPERAND_VALUE: {
        res.val = operand->val;
        res.version = operand->version;
        break;
    }
    case OPERAND_BBLOCK: {
        res.bblock = id_copy(operand->bblock);
        break;
    }
    case OPERAND_FUNC: {
        res.func = id_copy(operand->func);
        break;
    }
    case OPERAND_CONSTANT: {
        res.constant = operand->constant;
        break;
    }
    }
    
    return res;
}

static val_t* val_copy(bblock_t* bblock, const val_t* val) {
    val_t* res = malloc(sizeof(val_t));
    res->bblock = bblock;
    res->flags = val->flags;
    res->opcode = val->opcode;
    res->version = val->version;
    res->id = val->id;
    res->dtype = val->dtype;
    res->operand_count = val->operand_count;
    res->operands = malloc(val->operand_count*sizeof(operand_t));
    
    for (uint16_t i = 0; i < val->operand_count; i++)
        res->operands[i] = operand_copy(&val->operands[i]);
    
    return res;
}

static bblock_t* bblock_copy(func_t* func, const bblock_t* bblock) {
    bblock_t* res = malloc(sizeof(bblock_t));
    res->func = func;
    res->id = id_copy(bblock->id);
    res->flags = bblock->flags;
    res->val_count = bblock->val_count;
    res->vals = malloc(bblock->val_count*sizeof(val_t*));
    res->caller_count = bblock->caller_count;
    res->callee_count = bblock->callee_count;
    res->callers = malloc(bblock->caller_count*sizeof(bblock_t*));
    res->callees = malloc(bblock->callee_count*sizeof(bblock_t*));
    memcpy(res->callers, bblock->callers, bblock->caller_count*sizeof(bblock_t*));
    memcpy(res->callees, bblock->callees, bblock->callee_count*sizeof(bblock_t*));
    
    for (uint32_t i = 0; i < bblock->val_count; i++)
        res->vals[i] = val_copy(res, bblock->vals[i]);
    
    return res;
}

static func_t* func_copy(module_t* module, const func_t* func) {
    func_t* res = malloc(sizeof(func_t));
    res->module = module;
    res->id = id_copy(func->id);
    res->flags = func->flags;
    res->bblock_count = func->bblock_count;
    res->bblocks = malloc(func->bblock_count*sizeof(bblock_t*));
    
    for (uint32_t i = 0; i < func->bblock_count; i++)
        res->bblocks[i] = bblock_copy(res, func->bblocks[i]);
    
    return res;
}

module_t* module_copy(const module_t* module) {
    module_t* res = malloc(sizeof(module_t));
    res->flags = module->flags;
    res->func_count = module->func_count;
    res->funcs = malloc(module->func_count*sizeof(func_t*));
    
    for (uint32_t i = 0; i < module->func_count; i++)
        res->funcs[i] = func_copy(res, module->funcs[i]);
    
    return res;
}

static void add_bblock_caller(bblock_t* bblock, bblock_t* caller) {
    for (size_t i = 0; i < bblock->caller_count; i++)
        if (bblock->callers[i] == caller) return;
    
    bblock->callers = realloc(bblock->callers, (bblock->caller_count+1)*sizeof(bblock_t*));
    bblock->callers[bblock->caller_count++] = caller;
}

static void add_bblock_callee(bblock_t* bblock, bblock_t* callee) {
    for (size_t i = 0; i < bblock->callee_count; i++)
        if (bblock->callees[i] == callee) return;
    
    bblock->callees = realloc(bblock->callees, (bblock->callee_count+1)*sizeof(bblock_t*));
    bblock->callees[bblock->callee_count++] = callee;
}

static void create_bblock_graph(bblock_t* bblock) {
    val_t* last = bblock->vals[bblock->val_count-1];
    
    if (last->opcode == OPCODE_JUMP) {
        bblock_t* dest = func_find_basic_block(bblock->func, last->operands[0].bblock);
        add_bblock_caller(dest, bblock);
        add_bblock_callee(bblock, dest);
    } else if (last->opcode == OPCODE_JUMP_COND) {
        bblock_t* dest = func_find_basic_block(bblock->func, last->operands[1].bblock);
        add_bblock_caller(dest, bblock);
        add_bblock_callee(bblock, dest);
        
        dest = func_find_basic_block(bblock->func, last->operands[2].bblock);
        add_bblock_caller(dest, bblock);
        add_bblock_callee(bblock, dest);
    }
}

void module_create_bblock_call_graph(module_t* module) {
    if (module->flags & MODULE_BBLOCK_CALL_GRAPH) return;
    
    for (uint32_t i = 0; i < module->func_count; i++) {
        func_t* func = module->funcs[i];
        
        for (uint32_t j = 0; j < func->bblock_count; j++) {
            func->bblocks[j]->callee_count = 0;
            func->bblocks[j]->caller_count = 0;
            free(func->bblocks[j]->callees);
            free(func->bblocks[j]->callers);
            func->bblocks[j]->callees = NULL;
            func->bblocks[j]->callers = NULL;
        }
        
        for (uint32_t j = 0; j < func->bblock_count; j++)
            create_bblock_graph(func->bblocks[j]);
    }
    
    module->flags |= MODULE_BBLOCK_CALL_GRAPH;
}

void module_remove_bblock_call_graph(module_t* module) {
    if (!(module->flags&MODULE_BBLOCK_CALL_GRAPH)) return;
    
    for (uint32_t i = 0; i < module->func_count; i++) {
        func_t* func = module->funcs[i];
        for (uint32_t j = 0; j < func->bblock_count; j++) {
            func->bblocks[j]->callee_count = 0;
            func->bblocks[j]->caller_count = 0;
            free(func->bblocks[j]->callees);
            free(func->bblocks[j]->callers);
            func->bblocks[j]->callees = NULL;
            func->bblocks[j]->callers = NULL;
        }
    }
    
    module->flags &= ~MODULE_BBLOCK_CALL_GRAPH;
}

typedef struct to_ssa_state_t {
    uint32_t counter_count;
    uint32_t* counter_ids;
    uint32_t* counter_nexts;
} to_ssa_state_t;

static bool is_val_used_as_operand(bblock_t* bblock, uint32_t id) {
    for (uint32_t i = 0; i < bblock->val_count; i++) {
        val_t* val = bblock->vals[i];
        for (uint32_t j = 0; j < val->operand_count; j++)
            if (val->operands[j].type==OPERAND_VALUE && val->operands[j].val==id)
                return true;
    }
    return false;
}

static void find_latest_vals_in_bblock(bblock_t* bblock, uint32_t* count, val_t*** res) {
    uint32_t latest_count = 0;
    val_t** latest = NULL;
    
    for (uint32_t j = 0; j < bblock->val_count; j++) {
        val_t* val = bblock->vals[j];
        for (uint32_t k = 0; k < latest_count; k++) {
            if (latest[k]->id==val->id && latest[k]->version<val->version) {
                latest[k] = val;
                goto end;
            }
        }
        
        latest = realloc(latest, (latest_count+1)*sizeof(val_t*));
        latest[latest_count++] = val;
        
        end: ;
    }
    
    *count = latest_count;
    *res = latest;
}

static uint32_t create_phi_instructions(bblock_t* bblock) {
    uint32_t phi_count = 0;
    
    for (uint32_t i = 0; i < bblock->caller_count; i++) {
        bblock_t* caller = bblock->callers[i];
        uint32_t latest_count;
        val_t** latest;
        find_latest_vals_in_bblock(caller, &latest_count, &latest);
        
        for (uint32_t j = 0; j < latest_count; j++) {
            val_t* val = latest[j];
            
            if (!is_val_used_as_operand(bblock, val->id)) continue;
            
            for (uint32_t k = 0; k < phi_count; k++)
                if (bblock->vals[k]->id == val->id) goto end;
            
            val_new_at(bblock, 0, val->id, val->dtype, OPCODE_PHI, 0);
            phi_count++;
            
            end: ;
        }
        
        free(latest);
    }
    
    return phi_count;
}

static void populate_phi_instructions(bblock_t* bblock, uint32_t phi_count) {
    for (uint32_t i = 0; i < bblock->caller_count; i++) {
        bblock_t* caller = bblock->callers[i];
        uint32_t latest_count;
        val_t** latest;
        find_latest_vals_in_bblock(caller, &latest_count, &latest);
        
        for (uint32_t j = 0; j < latest_count; j++) {
            val_t* val = latest[j];
            
            if (!is_val_used_as_operand(bblock, val->id)) continue;
            
            for (uint32_t k = 0; k < phi_count; k++) {
                if (bblock->vals[k]->id == val->id) {
                    val_add_operand_bblock(bblock->vals[k], caller->id);
                    val_add_operand_val(bblock->vals[k], val->id, val->version);
                    goto end;
                }
            }
            
            end: ;
        }
        
        free(latest);
    }
}

static void do_bblock(to_ssa_state_t* state, bblock_t* bblock) {
    if (bblock->done) return;
    bblock->done = true;
    
    for (uint32_t i = 0; i < bblock->caller_count; i++)
        do_bblock(state, bblock->callers[i]);
    
    uint32_t phi_count = create_phi_instructions(bblock);
    
    //Update versions
    for (uint32_t i = 0; i < bblock->val_count; i++) {
        val_t* val = bblock->vals[i];
        
        for (uint32_t j = 0; j < val->operand_count && i>=phi_count; j++)
            if (val->operands[j].type == OPERAND_VALUE)
                for (uint32_t k = 0; k < state->counter_count; k++)
                    if (state->counter_ids[k] == val->operands[j].val)
                        val->operands[j].version = state->counter_nexts[k] - 1;
        
        for (uint32_t j = 0; j < state->counter_count; j++) {
            if (state->counter_ids[j] == val->id) {
                val->version = state->counter_nexts[j]++;
                goto end;
            }
        }
        
        state->counter_ids = realloc(state->counter_ids, (state->counter_count+1)*4);
        state->counter_nexts = realloc(state->counter_nexts, (state->counter_count+1)*4);
        state->counter_ids[state->counter_count] = val->id;
        state->counter_nexts[state->counter_count++] = 1;
        val->version = 0;
        
        end: ;
    }
    
    populate_phi_instructions(bblock, phi_count);
}

static void func_convert_to_ssa(func_t* func) {
    for (uint32_t i = 0; i < func->bblock_count; i++)
        func->bblocks[i]->done = false;
    
    to_ssa_state_t state;
    state.counter_count = 0;
    state.counter_ids = NULL;
    state.counter_nexts = NULL;
    
    for (uint32_t i = 0; i < func->bblock_count; i++)
        do_bblock(&state, func->bblocks[i]);
    
    free(state.counter_nexts);
    free(state.counter_ids);
}

void module_convert_to_ssa(module_t* module) {
    if (module->flags & MODULE_SSA) return;
    
    if (!(module->flags&MODULE_BBLOCK_CALL_GRAPH))
        module_create_bblock_call_graph(module);
    
    for (uint32_t i = 0; i < module->func_count; i++)
        func_convert_to_ssa(module->funcs[i]);
    
    module->flags |= MODULE_SSA;
}

static void replace_vals(func_t* func, uint32_t from, uint32_t to) {
    for (uint32_t i = 0; i < func->bblock_count; i++) {
        bblock_t* bblock = func->bblocks[i];
        for (uint32_t j = 0; j < bblock->val_count; j++) {
            val_t* val = bblock->vals[j];
            if (val->id == from) val->id = to;
            
            for (uint32_t k = 0; k < val->operand_count; k++)
                if (val->operands[k].type == OPERAND_VALUE &&
                    val->operands[k].val==from)
                    val->operands[k].val = to;
        }
    }
}

static bool is_val_assigned_to(bblock_t* bblock, uint32_t id, uint32_t ver) {
    for (uint32_t i = 0; i < bblock->val_count; i++) {
        if (bblock->vals[i]->id==id && bblock->vals[i]->version==ver)
            return true;
    }
    return false;
}

static void handle_phi(func_t* func, val_t* phi) {
    for (uint32_t i = 0; i < phi->operand_count; i+=2) {
        switch (phi->operands[i+1].type) {
        case OPERAND_VALUE: {
            bblock_t* bblock = func_find_basic_block(func, phi->operands[i].bblock);
            operand_t val = phi->operands[i+1];
            if (is_val_assigned_to(bblock, val.val, val.version)) {
                replace_vals(func, phi->operands[i+1].val, phi->id);
            } else {
                val_t* copy = val_new_at(bblock, bblock->val_count-1, phi->id, phi->dtype, OPCODE_COPY, 0);
                val_add_operand_val(copy, val.val, val.version);
            }
            break;
        }
        case OPERAND_BBLOCK:
        case OPERAND_FUNC:
        case OPERAND_CONSTANT: {
            bblock_t* bblock = func_find_basic_block(func, phi->operands[i].bblock);
            val_t* copy = val_new_at(bblock, bblock->val_count-1, phi->id, phi->dtype, OPCODE_COPY, 0);
            switch (phi->operands[i+1].type) {
            case OPERAND_BBLOCK:
                val_add_operand_bblock(copy, phi->operands[i+1].bblock);
                break;
            case OPERAND_FUNC:
                val_add_operand_func(copy, phi->operands[i+1].func);
                break;
            case OPERAND_CONSTANT:
                val_add_operand_const(copy, phi->operands[i+1].constant);
                break;
            case OPERAND_VALUE:
                assert(false);
                break;
            }
        }
        }
    }
    
    val_del(phi);
}

static void func_convert_from_ssa(func_t* func) {
    for (uint32_t i = 0; i < func->bblock_count; i++) {
        bblock_t* bblock = func->bblocks[i];
        
        for (uint32_t j = 0; j < bblock->val_count;) {
            val_t* val = bblock->vals[j];
            if (val->opcode == OPCODE_PHI) handle_phi(func, val);
            else j++;
        }
    }
}

void module_convert_from_ssa(module_t* module) {
    if (!(module->flags&MODULE_SSA)) return;
    
    for (uint32_t i = 0; i < module->func_count; i++)
        func_convert_from_ssa(module->funcs[i]);
    
    module->flags &= ~MODULE_SSA;
}
