#include "const_ops.h"

//TODO: Implement these

const_t const_add_wrap(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] + b.data[1];
    return res;
}

const_t const_add_saturate(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] + b.data[1];
    return res;
}

const_t const_subtract_wrap(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] - b.data[1];
    return res;
}

const_t const_subtract_saturate(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] - b.data[1];
    return res;
}

const_t const_multiply_wrap(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] * b.data[1];
    return res;
}

const_t const_multiply_saturate(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] * b.data[1];
    return res;
}

const_t const_divide_wrap(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] / b.data[1];
    return res;
}

const_t const_divide_saturate(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] / b.data[1];
    return res;
}

const_t const_modulo_wrap(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] % b.data[1];
    return res;
}

const_t const_modulo_saturate(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = 0;
    res.data[1] = a.data[1] % b.data[1];
    return res;
}

const_t const_or(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = a.data[0] | b.data[0];
    res.data[1] = a.data[1] | b.data[1];
    return res;
}

const_t const_xor(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = a.data[0] ^ b.data[0];
    res.data[1] = a.data[1] ^ b.data[1];
    return res;
}

const_t const_and(const_t a, const_t b) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = a.data[0] & b.data[0];
    res.data[1] = a.data[1] & b.data[1];
    return res;
}

const_t const_not(const_t a) {
    const_t res;
    res.dtype = a.dtype;
    res.data[0] = ~a.data[0];
    res.data[1] = ~a.data[1];
    return res;
}

bool const_eq(const_t a, const_t b) {
    if (!dtype_eq(a.dtype, b.dtype)) return false;
    if (a.data[0] != b.data[0]) return false;
    if (a.data[1] != b.data[1]) return false;
    return true;
}
