#ifndef MODULE_H
#define MODULE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef struct id_t id_t;
typedef struct dtype_t dtype_t;
typedef struct const_t const_t;
typedef struct operand_t operand_t;
typedef struct val_side_effect_t val_side_effect_t;
typedef struct val_t val_t;
typedef struct bblock_t bblock_t;
typedef struct func_t func_t;
typedef struct module_t module_t;

typedef enum opcode_t {
    OPCODE_ALLOCA,
    OPCODE_COPY,
    OPCODE_ADD,
    OPCODE_SUBTRACT,
    OPCODE_MULTIPLY,
    OPCODE_DIVIDE,
    OPCODE_MODULO,
    OPCODE_ARITH_LSHIFT,
    OPCODE_ARITH_RSHIFT,
    OPCODE_LOGICAL_LSHIFT,
    OPCODE_LOGICAL_RSHIFT,
    OPCODE_CIRCULAR_LSHIFT,
    OPCODE_CIRCULAR_RSHIFT,
    OPCODE_BITWISE_OR,
    OPCODE_BITWISE_XOR,
    OPCODE_BITWISE_AND,
    OPCODE_BITWISE_NOT,
    OPCODE_LOAD,
    //TODO: Loads that only work with the target's alignment
    //TODO: loads the always work
    //TODO: loads that may or may not work depending on the address, the target and the configuration of the target
    OPCODE_STORE,
    //TODO: String constants and stuff
    //TODO: Intrinsics
    OPCODE_CALL,
    OPCODE_JUMP,
    OPCODE_JUMP_COND,
    OPCODE_RETURN,
    OPCODE_PHI,
    OPCODE_ARG,
    OPCODE_CAST, //TODO: Change this
    OPCODE_REINTERPRET,
    OPCODE_LESS,
    OPCODE_GREATER,
    OPCODE_EQUAL
} opcode_t;

typedef enum base_dtype_t {
    DTYPE_VOID,
    DTYPE_INT,
    DTYPE_UINT,
    DTYPE_PTR, //An integer of unknown size
    DTYPE_OPAQUE //TODO: Extend this
} base_dtype_t;

typedef enum val_flags_t {
    //These two flags are mutually exclusive
    VAL_ARITH_WRAP = 1 << 0,
    VAL_ARITH_SATURATE = 1 << 1,
    VAL_HAS_SIDE_EFFECTS = 1 << 2
} val_flags_t;

typedef enum bblock_flags_t {
    BBLOCK_NONE = 0 //Unused
} bblock_flags_t;

typedef enum func_flags_t {
    FUNC_EXTERN = 1 << 0
} func_flags_t;

typedef enum module_flags_t {
    MODULE_BBLOCK_CALL_GRAPH = 1 << 0,
    MODULE_SSA = 1 << 1
} module_flags_t;

typedef enum operand_type_t {
    OPERAND_VALUE,
    OPERAND_BBLOCK,
    OPERAND_FUNC,
    OPERAND_CONSTANT,
} operand_type_t;

typedef enum val_side_effect_type_t {
    SIDE_EFFECT_UNSPECIFIED
} val_side_effect_type_t;

struct id_t {
    unsigned char* name;
};

struct dtype_t {
    base_dtype_t base:16;
    uint16_t bits; //For DTYPE_INT and DTYPE_UINT
};

struct const_t { //Value is data[1] + data[0]*(2**64)
    dtype_t dtype;
    uint64_t data[2];
};

struct operand_t {
    operand_type_t type;
    union {
        struct {uint32_t val; uint32_t version;};
        struct {id_t bblock;};
        struct {id_t func;};
        const_t constant;
    };
};

struct val_t {
    bblock_t* bblock;
    uint16_t flags;
    opcode_t opcode:16;
    uint32_t version;
    uint32_t id;
    dtype_t dtype;
    uint16_t operand_count;
    operand_t* operands;
};

struct val_side_effect_t {
    val_side_effect_type_t type;
};

struct bblock_t {
    func_t* func;
    id_t id;
    uint32_t flags;
    uint32_t val_count;
    val_t** vals;
    
    size_t caller_count;
    bblock_t** callers;
    size_t callee_count;
    bblock_t** callees;
    
    //Used only for SSA conversion
    bool done;
};

struct func_t {
    module_t* module;
    id_t id;
    uint32_t flags;
    uint32_t arg_count;
    dtype_t* arg_dtypes;
    dtype_t ret_dtype;
    uint32_t bblock_count;
    bblock_t** bblocks;
};

struct module_t {
    uint32_t flags;
    uint32_t func_count;
    func_t** funcs;
};

id_t id_new_nt(const char* str);
void id_del(id_t id);
id_t id_copy(const id_t id);
bool id_eq(const id_t a, const id_t b);

dtype_t dtype_new_int(uint16_t bits);
dtype_t dtype_new_uint(uint16_t bits);
dtype_t dtype_new_ptr();
dtype_t dtype_new_void();
dtype_t dtype_new_opaque();
bool dtype_eq(dtype_t a, dtype_t b);

const_t const_new_u32(uint32_t val);

val_t* val_new(bblock_t* bblock, uint32_t id, dtype_t dtype, opcode_t opcode, uint32_t flags);
val_t* val_new_at(bblock_t* bblock, uint32_t index, uint32_t id, dtype_t dtype, opcode_t opcode, uint32_t flags);
void val_del(val_t* val);
void val_add_operand(val_t* val, const operand_t operand);
void val_add_operand_val(val_t* val, uint32_t id, uint32_t version);
void val_add_operand_bblock(val_t* val, const id_t operand);
void val_add_operand_func(val_t* val, const id_t operand);
void val_add_operand_const(val_t* val, const const_t operand);
void val_set_operand(val_t* val, uint16_t index, const operand_t operand);
uint32_t val_get_side_effect_count(const val_t* val);
val_side_effect_t val_get_side_effect(const val_t* val, uint32_t index);

bblock_t* bblock_new(func_t* func, const id_t id, uint32_t flags);
void bblock_del(bblock_t* bblock);

func_t* func_new(module_t* module, const id_t id, dtype_t ret_dtype, uint32_t flags);
void func_add_arg(func_t* func, dtype_t dtype);
val_t* func_find_value(const func_t* func, const uint32_t id, int64_t version);
bblock_t* func_find_basic_block(const func_t* func, const id_t id);
void func_del(func_t* func);

module_t* module_new(uint32_t flags);
void module_del(module_t* module);
module_t* module_copy(const module_t* module);
func_t* module_find_func(const module_t* module, const id_t id);
void module_print(FILE* stream, const module_t* module);
module_t* module_scan(FILE* stream);
char* module_validate(const module_t* module);
void module_create_bblock_call_graph(module_t* module);
void module_remove_bblock_call_graph(module_t* module);
void module_convert_to_ssa(module_t* module);
void module_convert_from_ssa(module_t* module);
#endif
