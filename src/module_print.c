#include "module.h"

static unsigned int const_get_bit(const_t constant, uint16_t index) {
    uint16_t val_index = 1 - index / 64;
    uint16_t bit_index = index % 64;
    return (constant.data[val_index]&((uint64_t)1)<<bit_index) >> bit_index;
}

static void const_print(FILE* stream, const_t constant) {
    switch (constant.dtype.base) {
    case DTYPE_INT:
        fprintf(stream, "ci%u.0b", (unsigned int)constant.dtype.bits);
        break;
    case DTYPE_UINT:
        fprintf(stream, "cui%u.0b", (unsigned int)constant.dtype.bits);
        break;
    case DTYPE_PTR:
        fprintf(stream, "cptr%u.0b", (unsigned int)constant.dtype.bits);
        break;
    default:
        return;
    }
    
    uint32_t count = 1;
    for (uint16_t i = 0; i < constant.dtype.bits; i++)
        if (const_get_bit(constant, i)) count = i+1;
    
    for (int32_t i = count-1; i >= 0; i--)
        fprintf(stream, const_get_bit(constant, i) ? "1" : "0");
}

static void dtype_print(FILE* stream, dtype_t dtype) {
    switch (dtype.base) {
    case DTYPE_VOID:
        fprintf(stream, "void");
        break;
    case DTYPE_INT:
        fprintf(stream, "i%u", (unsigned int)dtype.bits);
        break;
    case DTYPE_UINT:
        fprintf(stream, "ui%u", (unsigned int)dtype.bits);
        break;
    case DTYPE_PTR:
        fprintf(stream, "ptr");
        break;
    case DTYPE_OPAQUE:
        fprintf(stream, "opaque");
        break;
    }
}

static void val_print(FILE* stream, const module_t* module, const val_t* val) {
    fprintf(stream, "        ");
    
    dtype_print(stream, val->dtype);
    fprintf(stream, " ");
    
    if (module->flags & MODULE_SSA)
        fprintf(stream, "val.%u(%u) = ", val->id, val->version);
    else
        fprintf(stream, "val.%u = ", val->id);
    
    switch (val->opcode) {
    case OPCODE_ALLOCA: fprintf(stream, "alloca"); break;
    case OPCODE_COPY: fprintf(stream, "copy"); break;
    case OPCODE_ADD: fprintf(stream, "add"); break;
    case OPCODE_SUBTRACT: fprintf(stream, "subtract"); break;
    case OPCODE_MULTIPLY: fprintf(stream, "multiply"); break;
    case OPCODE_DIVIDE: fprintf(stream, "divide"); break;
    case OPCODE_MODULO: fprintf(stream, "modulo"); break;
    case OPCODE_ARITH_LSHIFT: fprintf(stream, "alshift"); break;
    case OPCODE_ARITH_RSHIFT: fprintf(stream, "arshift"); break;
    case OPCODE_LOGICAL_LSHIFT: fprintf(stream, "llshift"); break;
    case OPCODE_LOGICAL_RSHIFT: fprintf(stream, "lrshift"); break;
    case OPCODE_CIRCULAR_LSHIFT: fprintf(stream, "clshift"); break;
    case OPCODE_CIRCULAR_RSHIFT: fprintf(stream, "crshift"); break;
    case OPCODE_BITWISE_OR: fprintf(stream, "or"); break;
    case OPCODE_BITWISE_XOR: fprintf(stream, "xor"); break;
    case OPCODE_BITWISE_AND: fprintf(stream, "and"); break;
    case OPCODE_BITWISE_NOT: fprintf(stream, "not"); break;
    case OPCODE_LOAD: fprintf(stream, "load"); break;
    case OPCODE_STORE: fprintf(stream, "store"); break;
    case OPCODE_CALL: fprintf(stream, "call"); break;
    case OPCODE_JUMP: fprintf(stream, "jump"); break;
    case OPCODE_JUMP_COND: fprintf(stream, "jump-cond"); break;
    case OPCODE_RETURN: fprintf(stream, "return"); break;
    case OPCODE_PHI: fprintf(stream, "phi"); break;
    case OPCODE_ARG: fprintf(stream, "arg"); break;
    case OPCODE_CAST: fprintf(stream, "cast"); break;
    case OPCODE_REINTERPRET: fprintf(stream, "reinterpret"); break;
    case OPCODE_LESS: fprintf(stream, "less"); break;
    case OPCODE_GREATER: fprintf(stream, "greater"); break;
    case OPCODE_EQUAL: fprintf(stream, "equal"); break;
    }
    
    if (val->flags) fprintf(stream, "(");
    if (val->flags & VAL_ARITH_WRAP)
        fprintf(stream, "wrap");
    if (val->flags&VAL_ARITH_WRAP && val->flags&(VAL_ARITH_SATURATE|VAL_HAS_SIDE_EFFECTS))
        fprintf(stream, " ");
    if (val->flags & VAL_ARITH_SATURATE)
        fprintf(stream, "saturate");
    if (val->flags&VAL_ARITH_SATURATE && val->flags&VAL_HAS_SIDE_EFFECTS)
        fprintf(stream, " ");
    if (val->flags & VAL_HAS_SIDE_EFFECTS)
        fprintf(stream, "has-side-effects");
    if (val->flags)
        fprintf(stream, ")");
    
    for (uint16_t i = 0; i < val->operand_count; i++) {
        fprintf(stream, " ");
        
        operand_t* op = &val->operands[i];
        switch (op->type) {
        case OPERAND_VALUE: {
            if (module->flags & MODULE_SSA)
                fprintf(stream, "val.%u(%u)", op->val, op->version);
            else
                fprintf(stream, "val.%u", op->val);
            break;
        }
        case OPERAND_BBLOCK: {
            fprintf(stream, "bblock.%s", op->bblock.name);
            break;
        }
        case OPERAND_FUNC: {
            fprintf(stream, "func.%s", op->func.name);
            break;
        }
        case OPERAND_CONSTANT: {
            const_print(stream, op->constant);
            break;
        }
        }
    }
    
    fprintf(stream, "\n");
}

static void bblock_print(FILE* stream, const module_t* module, const bblock_t* bblock) {
    fprintf(stream, "    bblock.%s", bblock->id.name);
    if (bblock->flags) fprintf(stream, "(");
    //
    if (bblock->flags) fprintf(stream, ")");
    fprintf(stream, ":\n");
    
    for (uint32_t i = 0; i < bblock->val_count; i++)
        val_print(stream, module, bblock->vals[i]);
}

static void func_print(FILE* stream, const module_t* module, const func_t* func) {
    fprintf(stream, "func.%s", func->id.name);
    if (func->flags) fprintf(stream, "(");
    if (func->flags & FUNC_EXTERN) fprintf(stream, "extern");
    if (func->flags) fprintf(stream, ")");
    fprintf(stream, ": ");
    dtype_print(stream, func->ret_dtype);
    if (func->arg_count) fprintf(stream, "(");
    for (uint32_t i = 0; i < func->arg_count; i++) {
        dtype_print(stream, func->arg_dtypes[i]);
        if (i != func->arg_count-1) fprintf(stream, " ");
    }
    if (func->arg_count) fprintf(stream, ")");
    fprintf(stream, "\n");
    
    for (uint32_t i = 0; i < func->bblock_count; i++)
        bblock_print(stream, module, func->bblocks[i]);
    
    fprintf(stream, "\n");
}

void module_print(FILE* stream, const module_t* module) {
    if (module->flags) {
        fprintf(stream, "flags:");
        if (module->flags & MODULE_SSA)
            fprintf(stream, " ssa");
        fprintf(stream, "\n");
    }
    
    for (uint32_t i = 0; i < module->func_count; i++)
        func_print(stream, module, module->funcs[i]);
}
