//A rather bad module parser
#include "module.h"

#include <string.h>
#include <stdarg.h>

static bool expect(FILE* stream, const char* str) {
    long int pos = ftell(stream);
    
    while (*str) {
        if (fgetc(stream) != *str) {
            fseek(stream, pos, SEEK_SET);
            return false;
        }
        str++;
    }
    
    return true;
}

static uint32_t get_ui32(FILE* stream) {
    uint32_t v;
    if (fscanf(stream, "%u", &v) != 1) ;//TODO: Handle
    return v;
}

static uint16_t get_ui16(FILE* stream) {
    uint16_t v;
    if (fscanf(stream, "%hu", &v) != 1) ;//TODO: Handle
    return v;
}

static void skip_newlines_and_spaces(FILE* stream) {
    while (true) {
        int c = fgetc(stream);
        if (c==' ' || c=='\t' || c=='\n') {
        } else {
            fseek(stream, ftell(stream)-1, SEEK_SET);
            return;
        }
    }
}

static void skip_spaces(FILE* stream) {
    while (true) {
        int c = fgetc(stream);
        if (c==' ' || c=='\t') {
        } else {
            fseek(stream, ftell(stream)-1, SEEK_SET);
            return;
        }
    }
}

static void const_set_bit(const_t* constant, uint16_t index) {
    uint16_t val_index = 1 - index / 64;
    uint16_t bit_index = index % 64;
    constant->data[val_index] |= ((uint64_t)1)<<bit_index;
}

static const_t scan_const(FILE* stream, uint16_t bits, dtype_t dtype) {
    const_t c;
    c.dtype = dtype;
    c.data[0] = c.data[1] = 0;
    
    size_t size = 0;
    int pos = ftell(stream);
    for (uint16_t index = 0; index < bits; index++) {
        if (expect(stream, "0") && !feof(stream)) size++;
        else if (expect(stream, "1")) size++;
        else if (expect(stream, " ") || expect(stream, "\t") ||
                 expect(stream, "\n")) break;
        else goto error;
    }
    fseek(stream, pos, SEEK_SET);
    
    for (uint16_t index = 0; index < bits; index++) {
        if (expect(stream, "0") && !feof(stream)) {
        } else if (expect(stream, "1")) {const_set_bit(&c, size-index-1);}
        else if (expect(stream, " ") || expect(stream, "\t") ||
                 expect(stream, "\n")) {
            fseek(stream, ftell(stream)-1, SEEK_SET);
            return c;
        }
    }
    
    return c;
    
    error: //TODO
        fprintf(stderr, "Scanning error!\n");
        return c;
}

static dtype_t dtype_scan(FILE* stream) {
    if (expect(stream, "void"))
        return dtype_new_void();
    else if (expect(stream, "i"))
        return dtype_new_int(get_ui16(stream));
    else if (expect(stream, "ui"))
        return dtype_new_uint(get_ui16(stream));
    else if (expect(stream, "ptr"))
        return dtype_new_ptr();
    else if (expect(stream, "opaque"))
        return dtype_new_opaque();
    else
        return dtype_new_void();
}

static void bblock_scan(FILE* stream, bblock_t* bblock) {
    while (!feof(stream)) {
        skip_newlines_and_spaces(stream);
        
        dtype_t dtype;
        if (expect(stream, "void"))
            dtype = dtype_new_void();
        else if (expect(stream, "i"))
            dtype = dtype_new_int(get_ui16(stream));
        else if (expect(stream, "ui"))
            dtype = dtype_new_uint(get_ui16(stream));
        else if (expect(stream, "ptr"))
            dtype = dtype_new_ptr();
        else if (expect(stream, "opaque"))
            dtype = dtype_new_opaque();
        else
            return;
        
        skip_spaces(stream);
        
        if (!expect(stream, "val.")) goto error;
        
        uint32_t id = get_ui32(stream);
        uint32_t ver = 0;
        skip_spaces(stream);
        if (expect(stream, "(")) {
            skip_spaces(stream);
            ver = get_ui32(stream);
            skip_spaces(stream);
            if (!expect(stream, ")")) goto error;
            skip_spaces(stream);
        }
        
        if (!expect(stream, "=")) goto error;
        
        skip_spaces(stream);
        
        opcode_t opcode;
        if (expect(stream, "alloca")) opcode = OPCODE_ALLOCA;
        else if (expect(stream, "copy")) opcode = OPCODE_COPY;
        else if (expect(stream, "add")) opcode = OPCODE_ADD;
        else if (expect(stream, "subtract")) opcode = OPCODE_SUBTRACT;
        else if (expect(stream, "multiply")) opcode = OPCODE_MULTIPLY;
        else if (expect(stream, "divide")) opcode = OPCODE_DIVIDE;
        else if (expect(stream, "modulo")) opcode = OPCODE_MODULO;
        else if (expect(stream, "alshift")) opcode = OPCODE_ARITH_LSHIFT;
        else if (expect(stream, "arshift")) opcode = OPCODE_ARITH_RSHIFT;
        else if (expect(stream, "llshift")) opcode = OPCODE_LOGICAL_LSHIFT;
        else if (expect(stream, "lrshift")) opcode = OPCODE_LOGICAL_RSHIFT;
        else if (expect(stream, "clshift")) opcode = OPCODE_CIRCULAR_LSHIFT;
        else if (expect(stream, "crshift")) opcode = OPCODE_CIRCULAR_RSHIFT;
        else if (expect(stream, "or")) opcode = OPCODE_BITWISE_OR;
        else if (expect(stream, "xor")) opcode = OPCODE_BITWISE_XOR;
        else if (expect(stream, "and")) opcode = OPCODE_BITWISE_AND;
        else if (expect(stream, "not")) opcode = OPCODE_BITWISE_NOT;
        else if (expect(stream, "load")) opcode = OPCODE_LOAD;
        else if (expect(stream, "store")) opcode = OPCODE_STORE;
        else if (expect(stream, "call")) opcode = OPCODE_CALL;
        else if (expect(stream, "jump-cond")) opcode = OPCODE_JUMP_COND;
        else if (expect(stream, "jump")) opcode = OPCODE_JUMP;
        else if (expect(stream, "return")) opcode = OPCODE_RETURN;
        else if (expect(stream, "phi")) opcode = OPCODE_PHI;
        else if (expect(stream, "arg")) opcode = OPCODE_ARG;
        else if (expect(stream, "cast")) opcode = OPCODE_CAST;
        else if (expect(stream, "reinterpret")) opcode = OPCODE_REINTERPRET;
        else if (expect(stream, "less")) opcode = OPCODE_LESS;
        else if (expect(stream, "greater")) opcode = OPCODE_GREATER;
        else if (expect(stream, "equal")) opcode = OPCODE_EQUAL;
        else goto error;
        
        skip_spaces(stream);
        
        uint32_t val_flags = 0;
        if (expect(stream, "(")) {
            skip_spaces(stream);
            
            while (true) {
                if (expect(stream, "wrap")) val_flags |= VAL_ARITH_WRAP;
                else if (expect(stream, "saturate"))
                    val_flags |= VAL_ARITH_SATURATE;
                else if (expect(stream, "has-side-effects"))
                    val_flags |= VAL_HAS_SIDE_EFFECTS;
                else if (expect(stream, ")")) break;
                else goto error;
                skip_spaces(stream);
            }
        }
        
        val_t* val = val_new(bblock, id, dtype, opcode, val_flags);
        val->version = ver;
        
        while (!feof(stream)) {
            skip_spaces(stream);
            
            if (expect(stream, "val.")) {
                uint32_t id = get_ui32(stream);
                uint32_t ver = 0;
                skip_spaces(stream);
                if (expect(stream, "(")) {
                    skip_spaces(stream);
                    ver = get_ui32(stream);
                    if (!expect(stream, ")")) goto error;
                }
                val_add_operand_val(val, id, ver);
            } else if (expect(stream, "bblock.")) {
                char name[1024];
                name[0] = 0;
                while (true) {
                    int c = fgetc(stream);
                    if (c==' ' || c=='\t' || c=='\n') {
                        fseek(stream, ftell(stream)-1, SEEK_SET);
                        break;
                    } else if (c == EOF) {
                        break;
                    }
                    char s[] = {c, 0};
                    strncat(name, s, 1024);
                }
                
                id_t id = id_new_nt(name);
                val_add_operand_bblock(val, id);
                id_del(id);
            } else if (expect(stream, "func.")) {
                char name[1024];
                name[0] = 0;
                while (true) {
                    int c = fgetc(stream);
                    if (c==' ' || c=='\t' || c=='\n') {
                        fseek(stream, ftell(stream)-1, SEEK_SET);
                        break;
                    } else if (c == EOF) {
                        break;
                    }
                    char s[] = {c, 0};
                    strncat(name, s, 1024);
                }
                
                id_t id = id_new_nt(name);
                val_add_operand_func(val, id);
                id_del(id);
            } else if (expect(stream, "ci")) {
                uint16_t bits = get_ui16(stream);
                if (!expect(stream, ".0b")) goto error;
                val_add_operand_const(val, scan_const(stream, bits, dtype_new_int(bits)));
            } else if (expect(stream, "cui")) {
                uint16_t bits = get_ui16(stream);
                if (!expect(stream, ".0b")) goto error;
                val_add_operand_const(val, scan_const(stream, bits, dtype_new_uint(bits)));
            } else if (expect(stream, "cptr")) {
                uint16_t bits = get_ui16(stream);
                if (!expect(stream, ".0b")) goto error;
                val_add_operand_const(val, scan_const(stream, bits, dtype_new_ptr()));
            } else {
                skip_spaces(stream);
                if (expect(stream, "\n")) goto next_inst;
                goto error;
            }
            
            skip_spaces(stream);
        }
        
        next_inst:
        skip_newlines_and_spaces(stream);
    }
    
    return;
    
    error: //TODO
        fprintf(stderr, "Scanning error!\n");
        return;
}

static void func_scan(FILE* stream, func_t* func) {
    while (true) {
        skip_newlines_and_spaces(stream);
        
        if (expect(stream, "bblock.")) {
            char name[1024];
            name[0] = 0;
            while (true) {
                int c = fgetc(stream);
                if (c == ':') break;
                else if (c == EOF) break;
                char s[] = {c, 0};
                strncat(name, s, 1024);
            }
            
            skip_spaces(stream);
            
            bblock_t* bblock = bblock_new(func, id_new_nt(name), 0);
            bblock_scan(stream, bblock);
        } else {
            if (feof(stream), fgetc(stream)) return;
            else fseek(stream, ftell(stream)-1, SEEK_SET);
            goto error;
        }
        
        skip_newlines_and_spaces(stream);
    }
    
    return;
    
    error: //TODO
        fprintf(stderr, "Scanning error!\n");
        return;
}

module_t* module_scan(FILE* stream) {
    module_t* module = module_new(0);
    
    while (true) {
        skip_newlines_and_spaces(stream);
        
        if (expect(stream, "func.")) {
            char name[1024];
            name[0] = 0;
            while (true) {
                int c = fgetc(stream);
                if (c==':' || c=='(') break;
                else if (c == EOF) break;
                char s[] = {c, 0};
                strncat(name, s, 1024);
            }
            
            skip_spaces(stream);
            
            uint32_t func_flags = 0;
            if (expect(stream, "(")) {
                skip_spaces(stream);
                
                while (true) {
                    if (expect(stream, "extern")) func_flags |= FUNC_EXTERN;
                    else if (expect(stream, "):")) break;
                    else goto error;
                    skip_spaces(stream);
                }
            }
            
            func_t* func = func_new(module, id_new_nt(name), dtype_scan(stream), func_flags);
            
            if (expect(stream, "(")) {
                skip_spaces(stream);
                
                while (true) {
                    skip_spaces(stream);
                    if (expect(stream, ")")) break;
                    dtype_t dtype = dtype_scan(stream);
                    func_add_arg(func, dtype);
                }
            }
            
            skip_newlines_and_spaces(stream);
            
            func_scan(stream, func);
        } else if (expect(stream, "flags:")) {
            skip_spaces(stream);
            
            while (true) {
                if (expect(stream, "ssa")) module->flags |= MODULE_SSA;
                else if (expect(stream, "\n")) break;
                else goto error;
                
                skip_spaces(stream);
            }
        } else {
            if (feof(stream), fgetc(stream)) return module;
            else fseek(stream, ftell(stream)-1, SEEK_SET);
            goto error;
        }
    }
    
    return module;
    
    error: //TODO
        fprintf(stderr, "Scanning error!\n");
        return NULL;
}
