#include "module.h"
#include "opt.h"

#include <stdlib.h>
#include <string.h>

static void validate(module_t* module) {
    char* res = module_validate(module);
    if (res) {
        fprintf(stderr, "Error: %s\n", res);
        module_print(stdout, module);
    }
}

int main(int argc, char** argv) {
    for (int i = 0; i < argc; i++)
        printf("%s ", argv[i]);
    printf("\n");
        
    FILE* f = fopen(argv[1], "r");
    module_t* module = module_scan(f);
    //module_print(stdout, module);
    fclose(f);
    
    validate(module);
    
    for (int i = 3; i < argc; i++) {
        if (!strcmp(argv[i], "cfp")) {
            cfp_callbacks callbacks;
            cfp_callbacks_wrap_fold(&callbacks);
            cfp_callbacks_default_test(&callbacks);
            cfp_run(module, &callbacks);
            validate(module);
        } else if (!strcmp(argv[i], "dce")) {
            dce_run(module);
            validate(module);
        } else if (!strcmp(argv[i], "die")) {
            die_run(module);
            validate(module);
        }
    }
    
    module_convert_from_ssa(module);
    
    for (int i = 3; i < argc; i++) {
        if (!strcmp(argv[i], "ssa")) {
            module_convert_to_ssa(module);
            validate(module);
        }
    }
    
    module_remove_bblock_call_graph(module);
    
    f = fopen(argv[2], "w");
    module_print(f, module);
    //printf("->\n\n");
    //module_print(stdout, module);
    fclose(f);
    
    module_del(module);
    
    return 0;
}
