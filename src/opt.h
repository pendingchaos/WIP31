#ifndef OPT_H
#define OPT_H

#include "module.h"

typedef struct cfp_callbacks cfp_callbacks;

struct cfp_callbacks {
    bool (*test)(const val_t* val, void* userdata);
    void* test_userdata;
    const_t (*fold)(const val_t* val, void* userdata);
    void* fold_userdata;
};

void cfp_callbacks_default_test(cfp_callbacks* callbacks);
void cfp_callbacks_wrap_fold(cfp_callbacks* callbacks);
void cfp_callbacks_saturate_fold(cfp_callbacks* callbacks);
void cfp_run(module_t* module, const cfp_callbacks* callbacks);

void dce_run(module_t* module);

void die_run(module_t* module);
#endif
