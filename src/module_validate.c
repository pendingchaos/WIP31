#include "module.h" 

#include <assert.h>

static char* validate_id(const id_t id) {
    for (unsigned char* c = id.name; *c; c++)
        if (*c==' ' || *c=='\n' || *c=='\t' || *c==':' || *c=='(' || *c==')')
            return "Invalid identifier name";
    return NULL;
}

static char* validate_operand(const operand_t* operand) {
    switch (operand->type) {
    case OPERAND_BBLOCK:
        return validate_id(operand->bblock);
    case OPERAND_CONSTANT:
        return NULL;
    case OPERAND_FUNC:
        return validate_id(operand->func);
    case OPERAND_VALUE:
        return NULL;
    default: {
        return "Invalid operand type";
    }
    }
}

static const val_t* lookup_val(const val_t* val, uint32_t operand_index) {
    const operand_t* operand = &val->operands[operand_index];
    if (operand->type == OPERAND_VALUE) {
        if (val->bblock->func->module->flags & MODULE_SSA)
            return func_find_value(val->bblock->func, operand->val, operand->version);
        else
            return func_find_value(val->bblock->func, operand->val, -1);
    }
    return NULL;
}

static bool valid_val(const val_t* val, uint32_t operand_index) {
    return lookup_val(val, operand_index) != NULL;
}

static bool valid_bblock(const val_t* val, uint32_t operand_index) {
    const operand_t* operand = &val->operands[operand_index];
    if (operand->type == OPERAND_BBLOCK)
        return func_find_basic_block(val->bblock->func, operand->bblock) != NULL;
    return false;
}

static bool valid_func(const val_t* val, uint32_t operand_index) {
    const operand_t* operand = &val->operands[operand_index];
    if (operand->type == OPERAND_FUNC)
        return module_find_func(val->bblock->func->module, operand->func) != NULL;
    return false;
}

static bool valid_constant(const val_t* val, uint32_t operand_index) {
    const operand_t* operand = &val->operands[operand_index];
    return operand->type == OPERAND_CONSTANT;
}

static dtype_t get_dtype(const val_t* val, uint32_t operand_index) {
    const operand_t* operand = &val->operands[operand_index];
    switch (operand->type) {
    case OPERAND_VALUE:
        return lookup_val(val, operand_index)->dtype;
    case OPERAND_CONSTANT:
        return operand->constant.dtype;
    case OPERAND_FUNC:
    case OPERAND_BBLOCK:
        return dtype_new_ptr();
    }
    assert(false);
}

static bool valid_operand(const val_t* val, uint32_t operand_index) {
    return valid_val(val, operand_index) ||
           valid_bblock(val, operand_index) ||
           valid_func(val, operand_index) ||
           valid_constant(val, operand_index);
}

static bool valid_integral_operand(const val_t* val, uint32_t operand_index) {
    if (!valid_operand(val, operand_index)) return false;
    dtype_t dtype = get_dtype(val, operand_index);
    switch (dtype.base) {
    case DTYPE_INT:
    case DTYPE_UINT: return dtype.bits>=1 && dtype.bits<=128;
    case DTYPE_PTR: return true;
    default: return false;
    }
}

static bool valid_boolean_operand(const val_t* val, uint32_t operand_index) {
    if (!valid_operand(val, operand_index)) return false;
    return dtype_eq(get_dtype(val, operand_index), dtype_new_uint(1));
}

static char* validate_val(const module_t* module, const val_t* val) { //TODO: SSA validation
    uint32_t allowed_flags = VAL_HAS_SIDE_EFFECTS;
    switch (val->opcode) {
    case OPCODE_ADD:
    case OPCODE_SUBTRACT:
    case OPCODE_MULTIPLY:
    case OPCODE_DIVIDE:
    case OPCODE_MODULO:
        allowed_flags |= VAL_ARITH_SATURATE | VAL_ARITH_WRAP;
        break;
    case OPCODE_ALLOCA:
    case OPCODE_COPY:
    case OPCODE_LOAD:
    case OPCODE_STORE:
    case OPCODE_CALL:
    case OPCODE_JUMP:
    case OPCODE_JUMP_COND:
    case OPCODE_RETURN:
    case OPCODE_PHI:
        break;
    default:
        return "Invalid opcode";
    }
    
    if (val->flags & ~allowed_flags) return "Invalid value flags";
    
    for (uint32_t i = 0; i < val->operand_count; i++) {
        char* res = validate_operand(&val->operands[i]);
        if (res) return res;
    }
    
    switch (val->opcode) { //TODO: Check to see if the value operands have been evaluated before used
    case OPCODE_ALLOCA: {
        if (val->operand_count != 1) return "Invalid alloca";
        break;
    }
    case OPCODE_COPY: {
        if (val->operand_count != 1) return "Invalid copy";
        if (val->dtype.base == DTYPE_VOID) return "Invalid copy";
        if (!valid_operand(val, 0) || !valid_operand(val, 0)) return "Invalid copy";
        if (!dtype_eq(get_dtype(val, 0), val->dtype)) return "Invalid copy";
        break;
    }
    case OPCODE_ADD:
    case OPCODE_SUBTRACT:
    case OPCODE_MULTIPLY:
    case OPCODE_DIVIDE:
    case OPCODE_MODULO:
    case OPCODE_BITWISE_OR:
    case OPCODE_BITWISE_XOR:
    case OPCODE_BITWISE_AND:
    case OPCODE_BITWISE_NOT:
    case OPCODE_ARITH_LSHIFT:
    case OPCODE_ARITH_RSHIFT:
    case OPCODE_LOGICAL_LSHIFT:
    case OPCODE_LOGICAL_RSHIFT:
    case OPCODE_CIRCULAR_LSHIFT:
    case OPCODE_CIRCULAR_RSHIFT: {
        if (val->operand_count != 2) return "Invalid arithmatic operation";
        if (!valid_integral_operand(val, 0) || !valid_integral_operand(val, 1))
            return "Invalid arithmatic operand";
        if (!dtype_eq(get_dtype(val, 0), get_dtype(val, 1))) return "Invalid arithmatic operation";
        if (!dtype_eq(get_dtype(val, 0), val->dtype)) return "Invalid arithmatic operation";
        break;
    }
    case OPCODE_LOAD: {
        if (val->operand_count != 1) return "Invalid load";
        if (val->dtype.base == DTYPE_VOID) return "Invalid load";
        if (!(valid_operand(val, 0) && get_dtype(val, 0).base==DTYPE_PTR)) return "Invalid load";
        break;
    }
    case OPCODE_STORE: {
        if (val->operand_count != 2) return "Invalid store";
        if (val->dtype.base != DTYPE_VOID) return "Invalid store";
        if (!(valid_operand(val, 0) && get_dtype(val, 0).base==DTYPE_PTR)) return "Invalid store";
        if (!valid_operand(val, 1)) return "Invalid store";
        break;
    }
    case OPCODE_CALL: {
        //TODO
        break;
    }
    case OPCODE_JUMP: {
        if (val->operand_count != 1) return "Invalid jump";
        if (val->dtype.base != DTYPE_VOID) return "Invalid jump";
        if (!valid_bblock(val, 0)) return "Invalid jump";
        break;
    }
    case OPCODE_JUMP_COND: {
        if (val->operand_count != 3) return "Invalid conditional jump";
        if (val->dtype.base != DTYPE_VOID) return "Invalid conditional jump";
        if (!valid_boolean_operand(val, 0)) return "Invalid conditional jump";
        if (!valid_bblock(val, 1)) return "Invalid conditional jump";
        if (!valid_bblock(val, 2)) return "Invalid conditional jump";
        break;
    }
    case OPCODE_RETURN: { //TODO: Check the type
        if (val->operand_count > 1) return "Invalid return";
        if (val->dtype.base != DTYPE_VOID) return "Invalid return";
        
        if (val->operand_count == 1)
            if (!valid_operand(val, 0)) return "Invalid return";
        break;
    }
    case OPCODE_PHI: {
        //if (!(module->flags&MODULE_SSA)) return "Phis are only allowed in SSA modules";
        if (val->operand_count%2) return "Invalid phi";
        if (val->dtype.base == DTYPE_VOID) return "Invalid phi";
        for (uint32_t i = 0; i < val->operand_count; i+=2) {
            if (!valid_operand(val, i+1)) return "Invalid phi";
            if (!valid_bblock(val, i)) return "Invalid phi";
            if (!dtype_eq(get_dtype(val, i+1), val->dtype)) return "Invalid phi";
        }
        break;
    }
    case OPCODE_ARG: {
        if (val->operand_count != 1) return "Invalid argument get operation";
        if (val->dtype.base == DTYPE_VOID) return "Invalid argument get operation";
        if (!valid_constant(val, 0)) return "Invalid argument get operation";
        if ((val->operands[0].constant.data[1]>=val->bblock->func->arg_count) ||
            val->operands[0].constant.data[0])
            return "Invalid argument get operation";
        break;
    }
    case OPCODE_CAST: {
        //TODO
        break;
    }
    case OPCODE_REINTERPRET: {
        //TODO
        break;
    }
    case OPCODE_LESS:
    case OPCODE_GREATER:
    case OPCODE_EQUAL: {
        if (val->operand_count != 2) return "Invalid comparison operation";
        if (val->dtype.base == DTYPE_VOID) return "Invalid comparison operation";
        if (!valid_integral_operand(val, 0) || !valid_integral_operand(val, 1))
            return "Invalid comparison operand";
        if (!dtype_eq(get_dtype(val, 0), get_dtype(val, 1))) return "Invalid comparison operation";
        if (!dtype_eq(val->dtype, dtype_new_uint(1))) return "Invalid comparison operation";
        break;
    }
    }
    
    return NULL;
}

static char* validate_bblock(const module_t* module, const bblock_t* bblock) {
    char* res = validate_id(bblock->id);
    if (res) return res;
    
    if (bblock->flags) return "Invalid basic block flags";
    
    if (bblock->val_count < 1) return "Basic blocks must have an end instruction";
    
    for (uint32_t i = 0; i < bblock->val_count; i++) {
        res = validate_val(module, bblock->vals[i]);
        if (res) return res;
    }
    
    return NULL;
}

static char* validate_func(const module_t* module, const func_t* func) {
    char* res = validate_id(func->id);
    if (res) return res;
    
    if (func->flags & ~FUNC_EXTERN) return "Invalid function flags";
    
    if (func->bblock_count < 1) return "Functions must have at least one basic block";
    
    for (uint32_t i = 0; i < func->bblock_count; i++) {
        res = validate_bblock(module, func->bblocks[i]);
        if (res) return res;
    }
    
    return NULL;
}

char* module_validate(const module_t* module) {
    for (uint32_t i = 0; i < module->func_count; i++) {
        char* res = validate_func(module, module->funcs[i]);
        if (res) return res;
    }
    
    return NULL;
}
