#include "opt.h"

#include <stdlib.h>

typedef struct dce_state_t {
    uint32_t marked_val_count;
    val_t** marked_vals;
    uint32_t marked_bblock_count;
    bblock_t** marked_bblocks;
} dce_state_t;

bool is_val_marked(dce_state_t* state, val_t* val) {
    for (uint32_t i = 0; i < state->marked_val_count; i++)
        if (state->marked_vals[i] == val) return true;
    return false;
}

bool is_bblock_marked(dce_state_t* state, bblock_t* bblock) {
    for (uint32_t i = 0; i < state->marked_bblock_count; i++)
        if (state->marked_bblocks[i] == bblock) return true;
    return false;
}

void mark_bblock(dce_state_t* state, func_t* func, bblock_t* bblock) {
    if (is_bblock_marked(state, bblock)) return;
    
    state->marked_bblocks = realloc(state->marked_bblocks, (state->marked_bblock_count+1)*sizeof(bblock_t*));
    state->marked_bblocks[state->marked_bblock_count++] = bblock;
    
    for (uint32_t i = 0; i < bblock->val_count; i++) {
        val_t* val = bblock->vals[i];
        for (uint32_t j = 0; j < val->operand_count; j++) {
            operand_t* op = &val->operands[j];
            if (op->type == OPERAND_BBLOCK)
                mark_bblock(state, func, func_find_basic_block(func, op->bblock));
        }
    }
}

void mark_val(dce_state_t* state, func_t* func, val_t* val) {
    if (is_val_marked(state, val)) return;
    
    state->marked_vals = realloc(state->marked_vals, (state->marked_val_count+1)*sizeof(val_t*));
    state->marked_vals[state->marked_val_count++] = val;
    
    for (uint32_t i = 0; i < val->operand_count; i++)
        if (val->operands[i].type == OPERAND_VALUE)
            mark_val(state, func, func_find_value(func, val->operands[i].val, val->operands[i].version));
}

void dce_run_func(func_t* func) {
    dce_state_t state;
    state.marked_val_count = 0;
    state.marked_vals = NULL;
    state.marked_bblock_count = 0;
    state.marked_bblocks = NULL;
    
    for (uint32_t i = 0; i < func->bblock_count; i++) {
        bblock_t* bblock = func->bblocks[i];
        for (uint32_t j = 0; j < bblock->val_count; j++) {
            val_t* val = bblock->vals[j];
            if (val->flags & VAL_HAS_SIDE_EFFECTS || val_get_side_effect_count(val))
                mark_val(&state, func, val);
        }
    }
    
    for (uint32_t i = 0; i < func->bblock_count; i++) {
        bblock_t* bblock = func->bblocks[i];
        for (uint32_t j = 0; j < bblock->val_count;) {
            val_t* val = bblock->vals[j];
            if (!is_val_marked(&state, val)) val_del(val);
            else j++;
        }
    }
    
    mark_bblock(&state, func, func->bblocks[0]);
    
    for (uint32_t i = 0; i < func->bblock_count;) {
        bblock_t* bblock = func->bblocks[i];
        if (!is_bblock_marked(&state, bblock)) bblock_del(bblock);
        else i++;
    }
    
    free(state.marked_bblocks);
    free(state.marked_vals);
}

void dce_run(module_t* module) {
    module_convert_to_ssa(module);
    
    for (uint32_t i = 0; i < module->func_count; i++)
        dce_run_func(module->funcs[i]);
}
