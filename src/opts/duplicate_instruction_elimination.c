#include "const_ops.h"
#include "module.h"

static bool duplicate(const val_t* a, const val_t* b) {
    if (a == b) return false; //Same instructions are not considered duplicates
    if (val_get_side_effect_count(a) || val_get_side_effect_count(b)) return false;
    if (a->flags != b->flags) return false;
    if (a->opcode != b->opcode) return false;
    if (!dtype_eq(a->dtype, b->dtype)) return false;
    if (a->operand_count != b->operand_count) return false;
    for (uint16_t i = 0; i < a->operand_count; i++) {
        operand_t* op0 = &a->operands[i];
        operand_t* op1 = &b->operands[i];
        if (op0->type != op1->type) return false;
        switch (op0->type) {
        case OPERAND_VALUE:
            if (op0->val != op1->val) return false;
            if (op0->version != op1->version) return false;
            break;
        case OPERAND_BBLOCK:
            if (!id_eq(op0->bblock, op1->bblock)) return false;
            break;
        case OPERAND_FUNC:
            if (!id_eq(op0->func, op1->func)) return false;
            break;
        case OPERAND_CONSTANT:
            if (!const_eq(op0->constant, op1->constant)) return false;
            break;
        }
    }
    
    return true;
}

typedef struct _val_iter_t {
    func_t* func;
    size_t bblock;
    size_t val;
} _val_iter_t;

_val_iter_t _val_iter_new(func_t* func) {
    return (_val_iter_t){func, 0, 0};
}

bool _val_iter_cond(const _val_iter_t* iter) {
    return (iter->bblock!=iter->func->bblock_count) &&
           (iter->val!=iter->func->bblocks[iter->bblock]->val_count);
}

void _val_iter_cont(_val_iter_t* iter) {
    iter->val++;
    if (iter->val == iter->func->bblocks[iter->bblock]->val_count) {
        iter->val = 0;
        iter->bblock++;
    }
}

#define FOREACHVAL(func, dest) for(_val_iter_t _##dest##iter=_val_iter_new((func));_val_iter_cond(&_##dest##iter);_val_iter_cont(&_##dest##iter))
#define FEV_VAL(dest) _##dest##iter.func->bblocks[_##dest##iter.bblock]->vals[_##dest##iter.val]
#define FEV_VAL_INDEX(dest) _##dest##iter.val
#define FEV_BBLOCK(dest) _##dest##iter.func->bblocks[_##dest##iter.bblock]
#define FEV_BBLOCK_INDEX(dest) _##dest##iter.bblock

//TODO: There is a duplicate of this in module.c
static void replace_vals(func_t* func, uint32_t from, uint32_t to) {
    for (uint32_t i = 0; i < func->bblock_count; i++) {
        bblock_t* bblock = func->bblocks[i];
        for (uint32_t j = 0; j < bblock->val_count; j++) {
            val_t* val = bblock->vals[j];
            if (val->id == from) val->id = to;
            
            for (uint32_t k = 0; k < val->operand_count; k++)
                if (val->operands[k].type == OPERAND_VALUE &&
                    val->operands[k].val==from)
                    val->operands[k].val = to;
        }
    }
}

static void _die_run(func_t* func) {
    FOREACHVAL(func, val1) {
        FOREACHVAL(func, val2) {
            if (duplicate(FEV_VAL(val1), FEV_VAL(val2))) { //TODO: Determine which one to delete
                                                           //Or maybe it should create a new one
                                                           //and delete both the old ones
                replace_vals(func, FEV_VAL(val2)->id, FEV_VAL(val1)->id);
                val_del(FEV_VAL(val2));
            }
        }
    }
}

void die_run(module_t* module) {
    module_convert_to_ssa(module);
    
    for (uint32_t i = 0; i < module->func_count; i++)
        _die_run(module->funcs[i]);
}
