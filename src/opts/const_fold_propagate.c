#include "const_ops.h"
#include "opt.h"

#include <assert.h>
#include <stdlib.h>

static const_t fold_wrap(const val_t* val, void* userdata);
static const_t fold_saturate(const val_t* val, void* userdata);

//TODO: Fold casts, reinterprets, comparisons and bit shifts
static const_t fold_wrap(const val_t* val, void* userdata) {
    if (val->flags & VAL_ARITH_SATURATE)
        return fold_saturate(val, NULL);
    
    operand_t* op = val->operands;
    switch (val->opcode) {
    case OPCODE_COPY:
        return op[0].constant;
    case OPCODE_ADD:
        return const_add_wrap(op[0].constant, op[1].constant);
    case OPCODE_SUBTRACT:
        return const_subtract_wrap(op[0].constant, op[1].constant);
    case OPCODE_MULTIPLY:
        return const_multiply_wrap(op[0].constant, op[1].constant);
    case OPCODE_DIVIDE:
        return const_divide_wrap(op[0].constant, op[1].constant);
    case OPCODE_MODULO:
        return const_modulo_wrap(op[0].constant, op[1].constant);
    case OPCODE_ARITH_LSHIFT:
    case OPCODE_ARITH_RSHIFT:
    case OPCODE_LOGICAL_LSHIFT:
    case OPCODE_LOGICAL_RSHIFT:
    case OPCODE_CIRCULAR_LSHIFT:
    case OPCODE_CIRCULAR_RSHIFT:
        assert(false);
    case OPCODE_BITWISE_OR:
        return const_or(op[0].constant, op[1].constant);
    case OPCODE_BITWISE_XOR:
        return const_xor(op[0].constant, op[1].constant);
    case OPCODE_BITWISE_AND:
        return const_and(op[0].constant, op[1].constant);
    case OPCODE_BITWISE_NOT:
        return const_not(op[0].constant);
    case OPCODE_LOAD:
    case OPCODE_STORE:
    case OPCODE_CALL:
    case OPCODE_JUMP:
    case OPCODE_JUMP_COND:
    case OPCODE_RETURN:
    case OPCODE_ALLOCA:
    case OPCODE_PHI:
    case OPCODE_ARG:
    case OPCODE_CAST:
    case OPCODE_REINTERPRET:
    case OPCODE_LESS:
    case OPCODE_GREATER:
    case OPCODE_EQUAL:
        assert(false);
    }
    
    assert(false);
}

static const_t fold_saturate(const val_t* val, void* userdata) {
    if (val->flags & VAL_ARITH_WRAP)
        return fold_wrap(val, NULL);
    
    operand_t* op = val->operands;
    switch (val->opcode) {
    case OPCODE_COPY:
        return op[0].constant;
    case OPCODE_ADD:
        return const_add_saturate(op[0].constant, op[1].constant);
    case OPCODE_SUBTRACT:
        return const_subtract_saturate(op[0].constant, op[1].constant);
    case OPCODE_MULTIPLY:
        return const_multiply_saturate(op[0].constant, op[1].constant);
    case OPCODE_DIVIDE:
        return const_divide_saturate(op[0].constant, op[1].constant);
    case OPCODE_MODULO:
        return const_modulo_saturate(op[0].constant, op[1].constant);
    case OPCODE_ARITH_LSHIFT:
    case OPCODE_ARITH_RSHIFT:
    case OPCODE_LOGICAL_LSHIFT:
    case OPCODE_LOGICAL_RSHIFT:
    case OPCODE_CIRCULAR_LSHIFT:
    case OPCODE_CIRCULAR_RSHIFT:
        assert(false);
    case OPCODE_BITWISE_OR:
        return const_or(op[0].constant, op[1].constant);
    case OPCODE_BITWISE_XOR:
        return const_xor(op[0].constant, op[1].constant);
    case OPCODE_BITWISE_AND:
        return const_and(op[0].constant, op[1].constant);
    case OPCODE_BITWISE_NOT:
        return const_not(op[0].constant);
    case OPCODE_LOAD:
    case OPCODE_STORE:
    case OPCODE_CALL:
    case OPCODE_JUMP:
    case OPCODE_JUMP_COND:
    case OPCODE_RETURN:
    case OPCODE_ALLOCA:
    case OPCODE_PHI:
    case OPCODE_ARG:
    case OPCODE_CAST:
    case OPCODE_REINTERPRET:
    case OPCODE_LESS:
    case OPCODE_GREATER:
    case OPCODE_EQUAL:
        assert(false);
    }
    
    assert(false);
}

void cfp_callbacks_wrap_fold(cfp_callbacks* callbacks) {
    callbacks->fold = &fold_wrap;
    callbacks->fold_userdata = NULL;
}

void cfp_callbacks_saturate_fold(cfp_callbacks* callbacks) {
    callbacks->fold = &fold_saturate;
    callbacks->fold_userdata = NULL;
}

static bool default_test(const val_t* val, void* userdata) {
    if (val->flags & VAL_HAS_SIDE_EFFECTS || val_get_side_effect_count(val)) return false;
    
    operand_t* op = val->operands;
    switch (val->opcode) {
    case OPCODE_COPY:
    case OPCODE_BITWISE_NOT:
        return op[0].type == OPERAND_CONSTANT;
    case OPCODE_ADD:
    case OPCODE_SUBTRACT:
    case OPCODE_MULTIPLY:
    case OPCODE_DIVIDE:
    case OPCODE_MODULO:
        return op[0].type == OPERAND_CONSTANT &&
               op[1].type == OPERAND_CONSTANT;
    case OPCODE_ARITH_LSHIFT:
    case OPCODE_ARITH_RSHIFT:
    case OPCODE_LOGICAL_LSHIFT:
    case OPCODE_LOGICAL_RSHIFT:
    case OPCODE_CIRCULAR_LSHIFT:
    case OPCODE_CIRCULAR_RSHIFT:
        return false;
    case OPCODE_BITWISE_OR:
    case OPCODE_BITWISE_XOR:
    case OPCODE_BITWISE_AND:
        return op[0].type == OPERAND_CONSTANT &&
               op[1].type == OPERAND_CONSTANT;
    case OPCODE_LOAD:
    case OPCODE_STORE:
    case OPCODE_CALL:
    case OPCODE_JUMP:
    case OPCODE_JUMP_COND:
    case OPCODE_RETURN:
    case OPCODE_ALLOCA:
    case OPCODE_PHI:
    case OPCODE_ARG:
    case OPCODE_CAST:
    case OPCODE_REINTERPRET:
    case OPCODE_LESS:
    case OPCODE_GREATER:
    case OPCODE_EQUAL:
        return false;
    }
    
    assert(false);
}

void cfp_callbacks_default_test(cfp_callbacks* callbacks) {
    callbacks->test = &default_test;
    callbacks->test_userdata = NULL;
}

static val_t** find_const_vals(const cfp_callbacks* callbacks, func_t* func, size_t* count) {
    size_t res_count = 0;
    val_t** res = NULL;
    
    for (uint32_t j = 0; j < func->bblock_count; j++) {
        bblock_t* block = func->bblocks[j];
        for (uint32_t k = 0; k < block->val_count; k++) {
            if (!callbacks->test(block->vals[k], callbacks->test_userdata))
                continue;
            
            res = realloc(res, (res_count+1)*sizeof(val_t*));
            res[res_count++] = block->vals[k];
        }
    }
    
    *count = res_count;
    return res;
}

static void _propagate_const_vals(val_t* val, size_t const_val_count,
                                  val_t** const_vals, const_t* folded) {
    for (uint16_t i = 0; i < val->operand_count; i++) {
        operand_t* op = &val->operands[i];
        
        for (size_t j = 0; j < const_val_count; j++) {
            val_t* const_val = const_vals[j];
            
            bool test = op->type==OPERAND_VALUE;
            test = test && op->val==const_val->id;
            test = test && op->version==const_val->version;
            if (test) {
                operand_t new_operand;
                new_operand.type = OPERAND_CONSTANT;
                new_operand.constant = folded[j];
                val_set_operand(val, i, new_operand);
                goto propagated;
            }
        }
        
        propagated:
            ;
    }
}

static void propagate_const_vals(func_t* func, size_t const_val_count,
                                 val_t** const_vals, const_t* folded) {
    for (uint32_t j = 0; j < func->bblock_count; j++) {
        bblock_t* block = func->bblocks[j];
        for (uint32_t k = 0; k < block->val_count; k++) {
            val_t* val = block->vals[k];
            
            _propagate_const_vals(val, const_val_count, const_vals, folded);
        }
    }
}

static void _cfp_run(func_t* func, const cfp_callbacks* callbacks) {
    size_t const_val_count = 0;
    val_t** const_vals = NULL;
    
    while (1) {
        const_vals = find_const_vals(callbacks, func, &const_val_count);
        if (const_val_count == 0) break;
        
        const_t* folded = malloc(const_val_count*sizeof(const_t));
        for (size_t i = 0; i < const_val_count; i++)
            folded[i] = callbacks->fold(const_vals[i], callbacks->fold_userdata);
        
        propagate_const_vals(func, const_val_count, const_vals, folded);
        
        free(folded);
        
        for (size_t i = 0; i < const_val_count; i++)
            val_del(const_vals[i]);
        
        free(const_vals);
    }

}

void cfp_run(module_t* module, const cfp_callbacks* callbacks) {
    module_convert_to_ssa(module);
    
    for (uint32_t i = 0; i < module->func_count; i++)
        _cfp_run(module->funcs[i], callbacks);
}
