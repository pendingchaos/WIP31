#!/bin/sh

cd ../; make -j9
cd tests

echo "Dead code elimination:"
valgrind --leak-check=full ../wip31 dce_test.ir .dce_test.ir dce
diff dce_test_expected.ir .dce_test.ir
rm -f .dce_test.ir

echo "Constant folding and propagation:"
valgrind --leak-check=full ../wip31 cfp_test.ir .cfp_test.ir cfp
diff cfp_test_expected.ir .cfp_test.ir
rm -f .cfp_test.ir

echo "Duplicate instruction elimination:"
valgrind --leak-check=full ../wip31 die_test.ir .die_test.ir die
diff die_test_expected.ir .die_test.ir
rm -f .die_test.ir

echo "To SSA:"
valgrind --leak-check=full ../wip31 to_ssa_test.ir .to_ssa_test.ir ssa
diff to_ssa_test_expected.ir .to_ssa_test.ir
rm -f .to_ssa_test.ir

echo "From SSA:"
valgrind --leak-check=full ../wip31 from_ssa_test.ir .from_ssa_test.ir ssa
diff from_ssa_test_expected.ir .from_ssa_test.ir
rm -f .from_ssa_test.ir
